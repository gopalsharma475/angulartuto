import { Component, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { UserslistComponent } from './users/userslist/userslist.component';
import { UsersaddComponent } from './users/usersadd/usersadd.component';
import { DetailsComponent } from './users/details/details.component';
import { UpdateComponent } from './users/update/update.component';
import { LoginComponent } from './users/login/login.component';
import { ContactusComponent } from './contact/contactus/contactus.component';
import { SubmitvalidationComponent } from './testfroms/submitvalidation/submitvalidation.component';
import { ForgotpasswordComponent } from './users/forgotpassword/forgotpassword.component';
import { ResetpasswordComponent } from './users/resetpassword/resetpassword.component';
import { SendotpComponent } from './users/sendotp/sendotp.component';
import { VerifyotpComponent } from './users/verifyotp/verifyotp.component';

const routes: Routes = [
  {
    path:"userslist",
    component: UserslistComponent
  },
  {
    path:"usersadd",
    component: UsersaddComponent
  },
  {
    path:"contactus",
    component: ContactusComponent
  },
  { 
    path: 'users/details/:userId',
    component: DetailsComponent 
  },
  { 
    path: 'users/update/:userId', 
    component: UpdateComponent 
  },
  { 
    path: 'users/update/:userId', 
    component: UpdateComponent 
  },
  { 
    path: 'test/submitvalidatio', 
    component: SubmitvalidationComponent 
  },
  {
    path:"login",
    component: LoginComponent
  },
  {
    path:"forgotpassword",
    component: ForgotpasswordComponent
  },
  {
    path:"resetpassword",
    component: ResetpasswordComponent
  },
  {
    path:"sendOtp",
    component: SendotpComponent
  },
  {
    path:"verified",
    component: VerifyotpComponent
  }

];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes),
    CommonModule
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
