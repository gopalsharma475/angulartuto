import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { NavBarService } from './nav-bar.service';
// import { HttpClient} from '@angular/common/http';
import * as alertyfy from 'alertifyjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angulartuto';
  constructor(private router: Router, public navBarService: NavBarService) {

  }

  logout() {
    alertyfy.success("Logout Success");
    this.navBarService.hideNavBar();
    this.router.navigateByUrl('login');
  }
}


