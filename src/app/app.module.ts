import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http'

import { AppComponent } from './app.component';
import { UserlistComponent } from './userlist/userlist.component';
import { ReactiveFormsModule,FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireModule } from '@angular/fire';
// Import  captcha
import { NgxCaptchaModule } from 'ngx-captcha';

//Import Custom modules 
import {ContactModule} from  './contact/contact.module';
import { UsersModule} from './users/users.module';
import { MatTableModule } from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
//import { MatTableDataSource } from '@angular/material/table';
// For Croper Start
import { LyTheme2, StyleRenderer, LY_THEME, LY_THEME_NAME, LyHammerGestureConfig} from '@alyle/ui';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HAMMER_GESTURE_CONFIG, HammerModule} from '@angular/platform-browser';
import { LyButtonModule} from '@alyle/ui/button';
import { LyToolbarModule } from '@alyle/ui/toolbar';
import { LyImageCropperModule } from '@alyle/ui/image-cropper';
import { CommonModule } from '@angular/common';
import { LySliderModule } from '@alyle/ui/slider';
import { LyIconModule } from '@alyle/ui/icon';
import { LyDialogModule } from '@alyle/ui/dialog';
import { CropperDialog } from './users/usersadd/cropper-dialog';
/** Import themes */
import { MinimaLight, MinimaDark } from '@alyle/ui/themes/minima';
import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDatepickerModule } from '@angular/material/datepicker'
import { MatNativeDateModule } from '@angular/material/core';
import { NavBarService } from './nav-bar.service';
// For Croper End

@NgModule({
  declarations: [
    AppComponent,
    UserlistComponent,
    CropperDialog
  ],
  imports: [
    MatTableModule,
    MatButtonModule,
    MatPaginatorModule,
    MatSortModule,
    MatIconModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
   // MatTableDataSource,
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    AppRoutingModule,
    ContactModule,
    UsersModule,
    NgxCaptchaModule,
    AngularFireModule.initializeApp({
      apiKey: "AIzaSyAMOzO5Gs0ATeGvXtQa1_m57jWbp6NcMWQ",
      authDomain: "angulartuto-88d98.firebaseapp.com",
      projectId: "angulartuto-88d98",
      storageBucket: "angulartuto-88d98.appspot.com",
      messagingSenderId: "501364889215",
      appId: "1:501364889215:web:488558091947ce30b5914a",
      measurementId: "G-KHVK5WVB5V"
    }),
    AngularFireStorageModule,
    BrowserAnimationsModule,
    LyButtonModule,
    LyToolbarModule,
    LyImageCropperModule,
    HammerModule,
    CommonModule,
    LySliderModule,
    LyIconModule,
    LyDialogModule
  ],
  providers: [
    [ LyTheme2 ],
    [ StyleRenderer ],
    // Theme that will be applied to this module
    { provide: LY_THEME_NAME, useValue: 'minima-light' },
    { provide: LY_THEME, useClass: MinimaLight, multi: true }, // name: `minima-light`
    { provide: LY_THEME, useClass: MinimaDark, multi: true }, // name: `minima-dark`
    // Gestures
    { provide: HAMMER_GESTURE_CONFIG, useClass: LyHammerGestureConfig }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
