import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root'
})

export class CommonService {

  constructor(private http: HttpClient) { }

  getUserList(){
    return this.http.get(environment.BASEURL+'users/list');
  }

  deleteUser(userid) {
    return this.http.post(environment.BASEURL+'users/delete',userid);
  }

  async userDetails(userid){
    await this.http.post(environment.BASEURL + 'users/detail', userid).subscribe(async (data) => {
      console.log("data>>>>>>>>>>", data);
      return await data['data'];
    });
  }
}
