import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContactusComponent } from './contactus/contactus.component';
//import {HttpClientModule} from  '@angular/common/http';
import { ReactiveFormsModule,FormsModule } from '@angular/forms';
import { NgxCaptchaModule } from 'ngx-captcha';



@NgModule({
  declarations: [ContactusComponent],
  imports: [
    CommonModule,
    //HttpClientModule,
    //FormGroup,
    //FormControl,
    ReactiveFormsModule,
    FormsModule,
    NgxCaptchaModule
  ],
  exports: [
    ContactusComponent
  ]
})
export class ContactModule { }
