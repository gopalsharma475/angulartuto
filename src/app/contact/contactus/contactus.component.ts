import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-contactus',
  templateUrl: './contactus.component.html',
  styleUrls: ['./contactus.component.css']
})
export class ContactusComponent implements OnInit {

  //siteKey:string = "6LcJ3p8aAAAAAE_sVgxesXTglsqAzHsmilBVHliR"; 
  siteKey:string = "6Lfr4Z8aAAAAANVHknVfm_MV3fPkv6n22xH4t3CE";
  firstAndLastNamePattern = '^[a-zA-Z0-9- ]*$' //^[a-zA-Z._-]+$
  emailValidationPattern = ".{1,}@[^.]{1,}";
  
  url = environment.BASEURL+'constactus/save';
  
  contactUsForm;
  submitted: boolean=false;
  constructor(private httpClient: HttpClient, private formBuild: FormBuilder) { 

    this.contactUsForm = this.formBuild.group({
      first_name: ['', Validators.required],
      last_name: ['', Validators.required],
      email: ['', Validators.required],
      subject: ['', Validators.required],
      message: ['', Validators.required],
      recaptcha: ['', Validators.required]
    })
  }

  get first_name(){ return this.contactUsForm.get('first_name')}
  get last_name(){ return this.contactUsForm.get('last_name')}
  get email(){ return this.contactUsForm.get('email')}
  get subject(){ return this.contactUsForm.get('subject')}
  get message(){ return this.contactUsForm.get('message')}
  
  contactUsAPI(values){
    this.submitted=true
    //alert(this.contactUsForm.status);
    if(this.contactUsForm.status != 'INVALID'){
      this.httpClient.post(this.url,values).subscribe((data)=>{
        console.warn("data>>>>>>>>>>>>>",data)
      })
    }else {
      
    }
    
  }

  ngOnInit(): void {

  }

  

}
