import { Injectable } from '@angular/core';
//import * as moment from 'moment';
import * as moment  from 'moment-timezone';

@Injectable({
  providedIn: 'root'
})
export class NavBarService {
  isHome : boolean = false;
  isContactUs : boolean = false;
  isUserList : boolean = false;
  isLogOut : boolean = false;
  userId : any;
  userLastlogin:any;
  userTimezone:any;
  UTCTime:any;
  utcTime:any;
  timeWithTimezone:any;
  constructor() { }

  showNavBar(){
    this.isHome = true;
    this.isContactUs = true;
    this.isLogOut = true;
    this.isUserList = true;
  }

  hideNavBar(){
    this.isHome = false;
    this.isContactUs = false;
    this.isLogOut = false;
    this.isUserList = false;
  }

  getTime(){
    this.userLastlogin = localStorage.getItem('lastLogin');
    //this.userTimezone = localStorage.getItem('timezone');
    this.userTimezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
    
    this.UTCTime = moment(this.userLastlogin);
    this.timeWithTimezone = this.UTCTime.tz(this.userTimezone).format('DD:MM:YYYY h:mm:ss a');
    return this.timeWithTimezone;  
  }

  getDisplayTime(dbTime){
    this.utcTime = dbTime;
    this.userTimezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
     
    
    this.UTCTime = moment(this.utcTime);
    this.timeWithTimezone = this.UTCTime.tz(this.userTimezone).format('DD:MM:YYYY h:mm:ss a');
    return this.timeWithTimezone; 
  }

  setUserDetailsLocalStorage(userData){
    this.userId = localStorage.setItem("userId",userData["data"]._id);
    this.userLastlogin = localStorage.setItem("lastLogin",userData["data"].last_login);
    this.userTimezone = localStorage.setItem("timezone",userData["data"].timezone);
  }

  deleteUserDetailsLocalStorage(){


  }
}
