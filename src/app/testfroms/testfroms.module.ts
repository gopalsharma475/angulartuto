import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SubmitvalidationComponent } from './submitvalidation/submitvalidation.component';
import { ReactiveFormsModule,FormsModule } from '@angular/forms';


@NgModule({
  declarations: [SubmitvalidationComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule
  ],
  exports: [
    SubmitvalidationComponent
  ]
})
export class TestfromsModule { }
