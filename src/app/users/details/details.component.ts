import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { CommonService } from 'src/app/common.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {
  params: Params;
  name;
  constructor(private activatedRoute: ActivatedRoute, private httpClient: HttpClient, private router: Router, private apiFunctions: CommonService) { }

  apiUrl = environment.BASEURL+'users/detail';
  apiData: any;
  ngOnInit(): void {

    this.activatedRoute.params.subscribe( async params => {
      const id = params['userId'];
      const userId = {"_id":id}
      this.httpClient.post(this.apiUrl,userId).subscribe((data)=>{
        this.apiData = data['data'];
      })
      // const userDetail = await this.apiFunctions.userDetails(userId);
      // console.log("userDetail>>>>>>>>>>>",userDetail);
      // this.apiData = await userDetail['data'];
    });
    

  }

}
