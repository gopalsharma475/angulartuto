import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router'; 
import * as alertyfy from 'alertifyjs';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-forgotpassword',
  templateUrl: './forgotpassword.component.html',
  styleUrls: ['./forgotpassword.component.css']
})
export class ForgotpasswordComponent implements OnInit {

  url = environment.BASEURL+'users/forgotPassword';
  //url = 'http://localhost:3000/api/v1/users/login';
  emailValidationPattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  path:String;
  userForgotForm: FormGroup;
  submitted: boolean = false;
  userForgotFormObj: any;
  localIsLogin : any;

  constructor(private httpClient: HttpClient, private formBuild: FormBuilder, private router: Router
    ) {
      this.userForgotForm = this.formBuild.group({
        email: ['', Validators.required], 
      })
   }

   get email(){ return this.userForgotForm.get('email')}

  ngOnInit(): void {
  }

  /**
   * @function :- forgot
   * @param value 
   */
  forgot(values){
    const formdata: FormData = new FormData();
    this.submitted=true;
    if(this.userForgotForm.status != 'INVALID'){
      this.httpClient.post(this.url,values).subscribe((data)=>{
        if(data["data"]==""){
          alertyfy.error(data["err"]); 
        }else{
          alertyfy.success(data["success"]);
          this.router.navigateByUrl('login');
        }
        
      })
    } else {
      alertyfy.error("All fields are required.");
    }
  }

}
