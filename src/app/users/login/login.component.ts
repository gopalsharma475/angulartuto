import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NavBarService } from 'src/app/nav-bar.service';
import * as alertyfy from 'alertifyjs';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  url = environment.BASEURL+'users/login';
  //url = 'http://localhost:3000/api/v1/users/login';
  //passwordPattern = "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$"
  passwordPattern = "";
  emailValidationPattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  path:String;
  userLoginForm: FormGroup;
  submitted: boolean = false;
  userloginFormObj: any;
  localIsLogin : any;

  constructor(private httpClient: HttpClient, private formBuild: FormBuilder, private router: Router, public navBarService: NavBarService) {

      this.userLoginForm = this.formBuild.group({
        email: ['', Validators.required],
        password: ['', Validators.required],
      })

  }

  get email(){ return this.userLoginForm.get('email')}
  get password(){ return this.userLoginForm.get('password')}

  ngOnInit(): void {
  }

  login(values){
    //const formData = new FormData();
    const formdata: FormData = new FormData();
    this.submitted=true;
    if(this.userLoginForm.status != 'INVALID'){
      this.userloginFormObj = this.userLoginForm.value;
      formdata.append('email',this.userloginFormObj.email);
      formdata.append('password',this.userloginFormObj.password);

      this.httpClient.post(this.url,values).subscribe((data)=>{
        if(data["data"]==""){
          alertyfy.error("Please check your loging details.");
          this.localIsLogin = localStorage.setItem("isLogin","no");
        }else{
          this.navBarService.showNavBar();
          this.navBarService.setUserDetailsLocalStorage(data);
          this.localIsLogin = localStorage.setItem("isLogin","yes");
          alertyfy.success("Login Successfully");
          this.router.navigateByUrl('userslist');
        }
        
      })
    }else {
      alertyfy.error("All fields are required.");  
    }
  }

  

}
