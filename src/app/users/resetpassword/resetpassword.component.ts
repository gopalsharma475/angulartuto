import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import * as alertyfy from 'alertifyjs';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-resetpassword',
  templateUrl: './resetpassword.component.html',
  styleUrls: ['./resetpassword.component.css']
})
export class ResetpasswordComponent implements OnInit {

  //url = environment.BASEURL+'users/resetPassword';
  url = 'http://localhost:3000/api/v1/users/resetPassword';
  passwordPattern = "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$"
  path:String;
  userResetForm: FormGroup;
  submitted: boolean = false;
  userResetFormObj: any;
  localIsLogin : any;
  constructor(private httpClient: HttpClient, private formBuild: FormBuilder, private router: Router) {
      this.userResetForm = this.formBuild.group({
        confirm_password: ['', Validators.required],
        password: ['', Validators.required],
      })
   }

  get confirm_password(){ return this.userResetForm.get('confirm_password')}
  get password(){ return this.userResetForm.get('password')}

  ngOnInit(): void {
  }

  resetPassword(values){
    const formdata: FormData = new FormData();
    this.submitted=true;
    if(this.userResetForm.status != 'INVALID'){
      values._id = localStorage.getItem("userId");
      this.userResetFormObj = this.userResetForm.value;
      
      this.httpClient.post(this.url,values).subscribe((data)=>{
        if(data["data"]==""){
          alertyfy.error(data["err"]); 
        }else{
          alertyfy.success(data["success"]);
        }
      })
    }else {
      alertyfy.error("All fields are required.");  
    }
  }

}
