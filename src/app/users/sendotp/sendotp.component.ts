import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NavBarService } from 'src/app/nav-bar.service';
import * as alertyfy from 'alertifyjs';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-sendotp',
  templateUrl: './sendotp.component.html',
  styleUrls: ['./sendotp.component.css']
})
export class SendotpComponent implements OnInit {

  url = environment.BASEURL+'users/sendSMS';
  // url = 'http://localhost:3000/api/v1/users/sendSMS';
  path:String;
  userSendOtpForm: FormGroup;
  submitted: boolean = false;
  userSendOtpFormObj: any;
  localIsLogin : any;

  constructor(private httpClient: HttpClient, private formBuild: FormBuilder, private router: Router, public navBarService: NavBarService) {

      this.userSendOtpForm = this.formBuild.group({
        phoneno: ['', Validators.required]
      })

  }

  get email(){ return this.userSendOtpForm.get('phoneno')}

  ngOnInit(): void {
  }

  sendOtp(values){
    //const formData = new FormData();
    const formdata: FormData = new FormData();
    this.submitted=true;
    if(this.userSendOtpForm.status != 'INVALID'){
      values._id = localStorage.getItem("userId");
      this.userSendOtpFormObj = this.userSendOtpForm.value;
      //return false;
      this.httpClient.post(this.url,values).subscribe((data)=>{
        if(data["data"]==""){
          alertyfy.error(data["err"]);
        }else{
          alertyfy.success(data["success"]);
          this.router.navigateByUrl('verified');
        }
      })
    }else {
      alertyfy.error("All fields are required.");  
    }
  }

}
