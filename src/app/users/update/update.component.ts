import { Component, OnInit, ViewChild, ElementRef, ChangeDetectorRef} from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { ImgCropperEvent } from '@alyle/ui/image-cropper';
import { LyDialog } from '@alyle/ui/dialog';
import { CropperDialog } from '../usersadd/cropper-dialog';
import * as alertyfy from 'alertifyjs';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css']
})
export class UpdateComponent implements OnInit {

  url = environment.BASEURL+'users/update';
  //mobNumberPattern = "^\([0-9]{3}\)[0-9]{3}-[0-9]{4}$";
  mobNumberPattern = /^(\()?\d{3}(\))?(-|\s)?\d{3}(-|\s)\d{4}$/;
  passwordPattern = "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$"
  firstAndLastNamePattern = '^[a-zA-Z0-9- ]*$' //^[a-zA-Z._-]+$
  //emailValidationPattern = "^[^\s@]+@[^\s@]+$";
  emailValidationPattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  path:String;
  //userAddForm;

  @ViewChild('UploadFileInput', { static: false }) uploadFileInput: ElementRef;
  userAddForm: FormGroup;
  fileInputLabel: string;
  updateUserId: string;
  userAddDataObj: any;
  fileToUpload: File = null;
  ImageUrl: string;
  imageChangedEvent: any = '';
  croppedImage: any = '';
  submitted: boolean=false;
  cropped?: string;
  ImageData: string;
  constructor(private activatedRoute: ActivatedRoute, private httpClient: HttpClient, private router: Router, private formBuild: FormBuilder,private _dialog: LyDialog,private _cd: ChangeDetectorRef) { 

    this.userAddForm = this.formBuild.group({
      first_name: ['', [Validators.required,Validators.maxLength(25)]],
      last_name: ['', Validators.required],
      email: ['', Validators.required],
      // password: ['', Validators.required],
      phoneno: ['', Validators.required],
      dob: ['', Validators.required],
      // uploadedImage: ['', Validators.required],
      uploadedImage: [''],
    })

  }

  get first_name(){ return this.userAddForm.get('first_name')}
  get last_name(){ return this.userAddForm.get('last_name')}
  get email(){ return this.userAddForm.get('email')}
  // get password(){ return this.userAddForm.get('password')}
  get phoneno(){ return this.userAddForm.get('phoneno')}
  get uploadedImage(){ return this.userAddForm.get('uploadedImage')}

  apiUrlUserDetails = environment.BASEURL+'users/detail';
  apiData: any;
  ngOnInit(): void {

    this.activatedRoute.params.subscribe(params => {
      const id = params['userId'];
      const userId = {"_id":id}
      this.updateUserId = id;
      this.httpClient.post(this.apiUrlUserDetails,userId).subscribe((data)=>{
        this.apiData = data['data'];
      })
    });

  }

  // Following code using for Alyle croper start
  /**
   * @function: openCropperDialog
   * @param event 
  */
 openCropperDialog(event: Event) {
    this.cropped = null!;
    this._dialog.open<CropperDialog, Event>(CropperDialog, {
      data: event,
      width: 320,
      disableClose: true
    }).afterClosed.subscribe((result?: ImgCropperEvent) => {
      if (result) {
        this.cropped = result.dataURL;
        this._cd.markForCheck();
        
      }
    });
  }

  /**
   * @function :- handleFileInput
   * @param $event 
   * @deprecated :- this function will call when file will change from view page
   */
  handleFileInput(file: FileList, event){
    this.imageChangedEvent = event;

    this.fileToUpload = file.item(0)
    var reader = new FileReader();
    reader.onload = (event: any) => {
      this.ImageUrl = event.target.result;
    }
    reader.readAsDataURL(this.fileToUpload)  
  }

  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;
  }

  userSaveAPI(values){
    //const formData = new FormData();
    const formdata: FormData = new FormData();
    this.submitted=true;
    if (typeof this.cropped === "undefined") {
      this.ImageData = "";
    } else {
      this.ImageData = this.cropped;
    }
    //return false;
    if(this.userAddForm.status != 'INVALID'){
      this.userAddDataObj = this.userAddForm.value;
      // formdata.append('uploadedImage',this.fileToUpload);
      formdata.append('first_name',this.userAddDataObj.first_name);
      formdata.append('last_name',this.userAddDataObj.last_name);
      formdata.append('email',this.userAddDataObj.email);
      formdata.append('password',this.userAddDataObj.password);
      formdata.append('phoneno',this.userAddDataObj.phoneno);
      formdata.append('dob',this.userAddDataObj.dob);
      formdata.append('_id',this.updateUserId);
      if (this.userAddForm.get('uploadedImage').value) {
        formdata.append('uploadedImage',this.fileToUpload);
      }
      formdata.append('base64image',this.ImageData);
      
      this.httpClient.post(this.url,formdata).subscribe((data)=>{
        alertyfy.success("User has been updated successfully.");
        this.router.navigateByUrl('/userslist');
      })
    }else {
      alertyfy.success("All fields are required.");
    }
  }

}
