import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserslistComponent } from './userslist/userslist.component';
import { UsersaddComponent } from './usersadd/usersadd.component';
import { ReactiveFormsModule,FormsModule } from '@angular/forms';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireModule } from '@angular/fire';
import { DetailsComponent } from './details/details.component';
import { UpdateComponent } from './update/update.component';
import { AppRoutingModule } from '../app-routing.module';
import { ImageCropperModule} from 'ngx-image-cropper';
import  { ConfirmationPopoverModule } from 'angular-confirmation-popover';
import { BrowserModule } from '@angular/platform-browser';
import { MatTableModule } from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatIconModule } from '@angular/material/icon'
//import { MatTableDataSource } from '@angular/material/table';
// For Croper Start
import { LyTheme2, StyleRenderer, LY_THEME, LY_THEME_NAME, LyHammerGestureConfig} from '@alyle/ui';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HAMMER_GESTURE_CONFIG, HammerModule} from '@angular/platform-browser';
import { LyButtonModule} from '@alyle/ui/button';
import { LyToolbarModule } from '@alyle/ui/toolbar';
import { LyImageCropperModule } from '@alyle/ui/image-cropper';
import { LyIconModule } from '@alyle/ui/icon';
import { LyDialogModule } from '@alyle/ui/dialog';
//import { CropperDialog } from './usersadd/cropper-dialog';
/** Import themes */
import { MinimaLight, MinimaDark } from '@alyle/ui/themes/minima';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule} from '@angular/material/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { LoginComponent } from './login/login.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { ForgotpasswordComponent } from './forgotpassword/forgotpassword.component';
import { ResetpasswordComponent } from './resetpassword/resetpassword.component';
import { SendotpComponent } from './sendotp/sendotp.component';
import { VerifyotpComponent } from './verifyotp/verifyotp.component';
// For Croper End


@NgModule({
  declarations: [UserslistComponent, UsersaddComponent, DetailsComponent, UpdateComponent, LoginComponent, NavBarComponent, ForgotpasswordComponent, ResetpasswordComponent, SendotpComponent, VerifyotpComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    AngularFireModule.initializeApp({
      apiKey: "AIzaSyAMOzO5Gs0ATeGvXtQa1_m57jWbp6NcMWQ",
      authDomain: "angulartuto-88d98.firebaseapp.com",
      projectId: "angulartuto-88d98",
      storageBucket: "angulartuto-88d98.appspot.com",
      messagingSenderId: "501364889215",
      appId: "1:501364889215:web:488558091947ce30b5914a",
      measurementId: "G-KHVK5WVB5V"
    }),
    AngularFireStorageModule,
    AppRoutingModule,
    ImageCropperModule,
    ConfirmationPopoverModule.forRoot({
      confirmButtonType: 'danger'
    }),
    BrowserModule,
    MatPaginatorModule,
    MatTableModule,
    MatButtonModule,
    MatSortModule,
    BrowserAnimationsModule,
    LyButtonModule,
    LyToolbarModule,
    LyImageCropperModule,
    LyIconModule,
    LyDialogModule,
    HammerModule,
    MatIconModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule
    //MatTableDataSource
  ],
  /** Add themes */
  providers: [
    [ LyTheme2 ],
    [ StyleRenderer ],
    // Theme that will be applied to this module
    { provide: LY_THEME_NAME, useValue: 'minima-light' },
    { provide: LY_THEME, useClass: MinimaLight, multi: true }, // name: `minima-light`
    { provide: LY_THEME, useClass: MinimaDark, multi: true }, // name: `minima-dark`
    // Gestures
    { provide: HAMMER_GESTURE_CONFIG, useClass: LyHammerGestureConfig }
  ],
  bootstrap: [UserslistComponent, UsersaddComponent, DetailsComponent, UpdateComponent],
  exports: [
    UserslistComponent,
    UsersaddComponent
  ]
})
export class UsersModule { }
