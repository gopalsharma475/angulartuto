import { Component, OnInit, ViewChild, ElementRef, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { AngularFireStorage } from '@angular/fire/storage'; 
import { Router } from '@angular/router';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { ImgCropperEvent } from '@alyle/ui/image-cropper';
import { LyDialog } from '@alyle/ui/dialog';
import { CropperDialog } from './cropper-dialog';
import { base64StringToBlob } from 'blob-util';
import * as alertyfy from 'alertifyjs';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-usersadd',
  templateUrl: './usersadd.component.html',
  styleUrls: ['./usersadd.component.css']
})
export class UsersaddComponent implements OnInit {
  cropped?: string;
  url = environment.BASEURL+'users/save';
  mobNumberPattern = /^(\()?\d{3}(\))?(-|\s)?\d{3}(-|\s)\d{4}$/; 
  passwordPattern = "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$"
  firstAndLastNamePattern = '^[a-zA-Z0-9- ]*$' //^[a-zA-Z._-]+$
  emailValidationPattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  path:String;
  //userAddForm;
  @ViewChild('UploadFileInput', { static: false }) uploadFileInput: ElementRef;
  userAddForm: FormGroup;
  fileInputLabel: string;
  userAddDataObj: any;
  fileToUpload: File = null;
  ImageUrl: string;
  imageChangedEvent: any = '';
  croppedImage: any = '';
  submitted: boolean = false; 
  
  constructor(private httpClient: HttpClient, private formBuild: FormBuilder, private af: AngularFireStorage, private router: Router,private _dialog: LyDialog,private _cd: ChangeDetectorRef) { 
    this.userAddForm = this.formBuild.group({
      first_name: ['', [Validators.required,Validators.maxLength(25)]],
      last_name: ['', Validators.required],
      email: ['', Validators.required],
      password: ['', Validators.required],
      phoneno: ['', Validators.required],
      uploadedImage: ['', Validators.required],
      dob: ['', Validators.required],
    })
  }

  get first_name(){ return this.userAddForm.get('first_name')}
  get last_name(){ return this.userAddForm.get('last_name')}
  get email(){ return this.userAddForm.get('email')}
  get password(){ return this.userAddForm.get('password')}
  get phoneno(){ return this.userAddForm.get('phoneno')}
  get uploadedImage(){ return this.userAddForm.get('uploadedImage')}
  get dob(){ return this.userAddForm.get('dob')}

  // Following code using for Alyle croper start
  /**
   * @function: openCropperDialog
   * @param event 
  */
  openCropperDialog(event: Event) {
    this.cropped = null!;
    this._dialog.open<CropperDialog, Event>(CropperDialog, {
      data: event,
      width: 320,
      disableClose: true
    }).afterClosed.subscribe((result?: ImgCropperEvent) => {
      if (result) {
        this.cropped = result.dataURL;
        this._cd.markForCheck();
        
      }
    });
  }
  // Following code using for Alyle croper start
  /**
   * @function :- handleFileInput
   * @param $event 
   * @deprecated :- this function will call when file will change from view page
   */
  handleFileInput(file: FileList, event){
      this.imageChangedEvent = event;

      this.fileToUpload = file.item(0)
      var reader = new FileReader();
      reader.onload = (event: any) => {
        this.ImageUrl = event.target.result;
      }
      reader.readAsDataURL(this.fileToUpload)  
  }

  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;
  }

  userSaveAPI(values){
    //const formData = new FormData();
    const formdata: FormData = new FormData();
    this.submitted=true;
    if(this.userAddForm.status != 'INVALID'){
      this.userAddDataObj = this.userAddForm.value;
      formdata.append('uploadedImage',this.fileToUpload);
      formdata.append('first_name',this.userAddDataObj.first_name);
      formdata.append('last_name',this.userAddDataObj.last_name);
      formdata.append('email',this.userAddDataObj.email);
      formdata.append('password',this.userAddDataObj.password);
      formdata.append('phoneno',this.userAddDataObj.phoneno);
      formdata.append('dob',this.userAddDataObj.dob);
      formdata.append('base64image',this.cropped);
      if (!this.userAddForm.get('uploadedImage').value) {
        alertyfy.error('Please fill valid details!');
        return false;
      } 

      // this line will upload image file in firebase
      //this.af.upload("/files/"+Math.random(),this.path)

      this.httpClient.post(this.url,formdata).subscribe((data)=>{
        console.warn("data>>>>>>>>>>>>>",data)
        alertyfy.success("User has been added successfully.");
        this.router.navigateByUrl('/userslist');
      })
    }else {
      alertyfy.error("All fields required.");
    }
  }

  ngOnInit(): void {
  }

}
