import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { PageEvent, MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { environment } from '../../../environments/environment';
import { NavBarService } from 'src/app/nav-bar.service';
import { CommonService } from 'src/app/common.service';

@Component({
  selector: 'app-userslist',
  templateUrl: './userslist.component.html',
  styleUrls: ['./userslist.component.css']
})
export class UserslistComponent implements OnInit {
  
  dataSource: MatTableDataSource<UserData>;

  displayedColumns = ['ID', 'FirstName', 'LastName', 'Email','PhoneNo','actions2'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  List: UserData[] = [];
  //List: any;
  isLoading: boolean;
  timeAccordingToTimezone : any;
  constructor(private httpClient: HttpClient, private router: Router, public navBarService: NavBarService, private apiFunctions: CommonService) { 
    this.timeAccordingToTimezone = this.navBarService.getTime(); 
  }

  apiUrl = environment.BASEURL+'users/list';
  deleteUserAPI = environment.BASEURL+'users/delete';
  apiData;
  public popoverTitle:string = 'Delete Confirmation';
  public popoverMessage:string = 'Do you want to delete user?';
  public confirmClicked:boolean =  false;
  public cancelClicked:boolean = false;
  ngOnInit() {
    this.userlist();
  }

  userlist(){
    //this.httpClient.get(this.apiUrl).subscribe(
      this.apiFunctions.getUserList().subscribe(
      async (res:any) => {
        this.List = res.data;
        this.isLoading = false;
        this.dataSource = new MatTableDataSource(this.List);
        //setTimeout(() => this.dataSource.paginator = this.paginator);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },
      (err) => {
        console.log("err>>>>>>>>",err);
      }
    );
  }

  /**
   * @function :- delete
   * @param $event 
   * @deprecated :- this function will call when file will change from view page
  */
  delete($event){
    const userId = {"userId":$event}
    //this.httpClient.post(this.deleteUserAPI,userId).subscribe((data)=>{
    this.apiFunctions.deleteUser(userId).subscribe((data)=>{
      this.userlist();
    })
    return false;
  }

  async getReport() {
    const jsonUrl = environment.BASEURL+'users/list';
    const res = await fetch(jsonUrl);
    const json = await res.json();
    const data =  json.data.map(row => ({
      first_name: row.first_name,
      last_name: row.last_name,
      email: row.email,
      phoneno:row.phoneno
    }));
    const csvData = this.objectToCsv(data);
    this.download(csvData);
  }
  
  objectToCsv = async (data) => {
    const csvRows = [];
    //get the headers
    const headers = Object.keys(data[0]);
    csvRows.push(headers.join(',')); 

    //loop over the rows
    for (const row of data) {
      const values = headers.map(header => {
        
        const escaped = (''+row[header]).replace(/"/g,'\\"')
        return `"${escaped}"`;
      })
      csvRows.push(values.join(','));
    } 
    // from ascaped comma separated values
    return csvRows.join('\n');
    //return csvRows;
  }

  download = async (data) => { 
    const blob = new Blob([data.__zone_symbol__value], {type: 'text/csv'});
    const url  =  window.URL.createObjectURL(blob);
    const a  = document.createElement('a');
    a.setAttribute('hidden','');
    a.setAttribute('href', url);
    a.setAttribute('download','userlist.csv');
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
  }

}


export interface UserData {
  _id:string
  first_name:string
  last_name:string
  gmail:string
  phoneno:string
}
