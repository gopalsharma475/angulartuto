import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NavBarService } from 'src/app/nav-bar.service';
import * as alertyfy from 'alertifyjs';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-verifyotp',
  templateUrl: './verifyotp.component.html',
  styleUrls: ['./verifyotp.component.css']
})
export class VerifyotpComponent implements OnInit {

  url = environment.BASEURL+'users/verifiedOTP';
  //url = 'http://localhost:3000/api/v1/users/verifiedOTP';
  path:String;
  userVerifiedOtpForm: FormGroup;
  submitted: boolean = false;
  userVerifiedOtpFormObj: any;
  localIsLogin : any;

  constructor(private httpClient: HttpClient, private formBuild: FormBuilder, private router: Router, public navBarService: NavBarService) {

      this.userVerifiedOtpForm = this.formBuild.group({
        otp: ['', Validators.required]
      })

  }

  get email(){ return this.userVerifiedOtpForm.get('otp')}

  ngOnInit(): void {
  }

  verifiedOtp(values){
    //const formData = new FormData();
    const formdata: FormData = new FormData();
    this.submitted=true;
    if(this.userVerifiedOtpForm.status != 'INVALID'){
      values._id = localStorage.getItem("userId");
      //return false;
      this.userVerifiedOtpFormObj = this.userVerifiedOtpForm.value;
      //return false;
      this.httpClient.post(this.url,values).subscribe((data)=>{
        if(data["data"]==""){
          alertyfy.error(data["err"]);
        }else{
          alertyfy.success(data["success"]);
          this.router.navigateByUrl('verified');
        }
      })
    }else {
      alertyfy.error("All fields are required.");  
    }
  }

}
